#include "matrixElement.h"
#include <algorithm>
#include <cstddef>
#include <iostream>
#include <istream>
#include <iterator>
#include <ostream>
#include <string>
#include <stdexcept> // std::out_of_range()
#include <iomanip>   // std::setw()
#include <utility>

using namespace std;

#include "matrix.h"

TwoDimensionMatrix::TwoDimensionMatrix() {
    std::fill(*matrix_, *matrix_ + this->size() * this->size(), 0);
}

TwoDimensionMatrix::TwoDimensionMatrix(const MatrixElement array[size_][size_]){
    std::copy(*array, *array + this->size() * this->size(), *matrix_);
}

TwoDimensionMatrix::TwoDimensionMatrix(const TwoDimensionMatrix& a){
    std::copy(*a.matrix_, *a.matrix_ + this->size() * this->size(), *matrix_);
}

MatrixElement TwoDimensionMatrix::get(size_t x, size_t y) const {
    return matrix_[x][y];
}

void TwoDimensionMatrix::set(size_t x, size_t y, MatrixElement i){
    matrix_[x][y] = i;
}


TwoDimensionMatrix &TwoDimensionMatrix::operator=(const TwoDimensionMatrix &a) {
    std::copy(*a.matrix_, *a.matrix_ + this->size() * this->size(), *matrix_);
    return *this;
}

MatrixElement *TwoDimensionMatrix::operator[](size_t i)  {
    return matrix_[i];
}

const MatrixElement *TwoDimensionMatrix::operator[](size_t i) const {
    return matrix_[i];
}

std::ostream& operator<<(std::ostream& os, const TwoDimensionMatrix &a){
    for(size_t x = 0; x < a.size(); x++){
        for(size_t y = 0; y < a.size(); y++){
            os << a.get(x, y) << " ";}
        os << "\n";
    }

    return os;
}

std::istream& operator>>(std::istream& is,TwoDimensionMatrix &a){
    int value;

    for(size_t i = 0; i < a.size(); i++){
        for(size_t j = 0; j < a.size(); j++){
            is >> value;
            a.set(i, j, value);
        }
    }
    return  is;
};

TwoDimensionMatrix operator+(const TwoDimensionMatrix&a, const TwoDimensionMatrix& b){
    TwoDimensionMatrix newMatrix;

    for(size_t i = 0; i < a.size(); i++){
        for(size_t j = 0; j < a.size(); j++){
            (newMatrix).set(i,j ,a.get(i,j) + b.get(i,j));
        }
    }

    return newMatrix;
}

TwoDimensionMatrix& TwoDimensionMatrix::operator*=(MatrixElement number){
    for(size_t i = 0; i < this->size(); i++){
        for(size_t j = 0; j < this->size(); j++){
            this->set(i,j ,(this->get(i,j) * number));
        }
    }

    return *this;
}

TwoDimensionMatrix TwoDimensionMatrix::operator&&(const TwoDimensionMatrix& matrix) const{
    TwoDimensionMatrix newMatrix;

    for(size_t i = 0; i < this->size(); i++){
        for(size_t j = 0; j < this->size(); j++){
            newMatrix.set(i,j ,matrix.get(i,j) && this->get(i, j));
        }
    }

    return newMatrix;
}