Dear Student,

I'm happy to announce that you've managed to get **12** out of 12 points for this assignment.

There still exist some issues that should be addressed before the deadline: **2023-04-22 23:50:00 CEST (+0200)**. For further details, please refer to the following list:

<details><summary>Cppcheck znalazł potencjalne błędy (to narzędzie może się pomylić)</summary>/tmp/tmp82v2spko/student/zaj2Matrix/matrix.h:93:5: warning: Class 'TwoDimensionMatrix' has a constructor with 1 argument that is not explicit. [noExplicitConstructor]<br>&nbsp;&nbsp;&nbsp;&nbsp;TwoDimensionMatrix(const MatrixElement[size_][size_]);<br>&nbsp;&nbsp;&nbsp;&nbsp;^<br></details>

-----------
I remain your faithful servant\
_Bobot_